﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IS445ProjectJLR.Models
{
    public class PhoneNumberModel
    {
        //Not using these attributes here because I'm experimenting with
        //writing the client side validation myself.
        //[DisplayName("Phone Number")]
        //[Required]
        public string PhoneNumber { get; set; }
    }
}