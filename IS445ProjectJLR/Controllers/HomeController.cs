﻿using IS445ProjectJLR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace IS445ProjectJLR.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Home/WebFormDemo/

        public ActionResult WebFormDemo()
        {
            return View();
        }

        //
        // GET: /Home/AbsoluteLayoutDemo/

        public ActionResult AbsoluteLayoutDemo()
        {
            ViewBag.Title = "Absolute Layout Demo";
            ViewBag.LayoutType = "absolute";
            return View("LayoutDemo");
        }

        //
        // GET: /Home/StaticLayoutDemo/

        public ActionResult StaticLayoutDemo()
        {
            ViewBag.Title = "Static Layout Demo";
            ViewBag.LayoutType = "static";
            return View("LayoutDemo");
        }

        //
        // GET: /Home/FloatLayoutDemo/

        public ActionResult FloatLayoutDemo()
        {
            ViewBag.Title = "Float Layout Demo";
            ViewBag.LayoutType = "float";
            return View("LayoutDemo");
        }

        //
        // GET: /Home/ClientSideDemo/

        public ActionResult ClientSideDemo()
        {
            return View();
        }

        //
        // GET: /Home/ServerSideDemo

        [HttpGet]
        public ActionResult ServerSideDemo()
        {
            ViewBag.Title = "Server Side Demo";
            PhoneNumberModel phoneNumModel = new PhoneNumberModel();

            return View(phoneNumModel);
        }

        //
        // POST: /Task/ServerSideDemo

        [HttpPost]
        public ActionResult ServerSideDemo(PhoneNumberModel phoneNumModel)
        {
            //When using model binding, always check to see if the model state is valid
            //before proceeding.
            if (ModelState.IsValid)
            {
                //Set up a regex to pull the numbers out of the phone number string.
                //We'll take all of the numbers and format them in the correct way.
                string regexPattern = @"[0-9]+";
                string inputString = phoneNumModel.PhoneNumber;
                StringBuilder outputSB = new StringBuilder();
                MatchCollection regexMatchColl = Regex.Matches(inputString, regexPattern);

                foreach (Match regexMatch in regexMatchColl)
                {
                    outputSB.Append(regexMatch.Value);
                }

                //If we've got less than 10 numbers, we can't proceed.
                if (outputSB.Length != 10)
                {
                    //We've got a problem.  Return to the user and let them know that they didn't submit
                    //a valid 10 digit phone number
                    ModelState.AddModelError("PhoneNumber", "The phone number must have 10 digits");

                    ViewBag.Title = "Server Side Demo";

                    return View(phoneNumModel);
                }

                //So far so good; let's format the phone number.
                else
                {
                    //Start with:
                    //xxxxxxxxxx
                    //0123456789  <--Index
                    outputSB.Insert(0, "(");

                    //Now we have:
                    //(xxxxxxxxxx
                    //01234567890
                    outputSB.Insert(4, ") ");

                    //Now we have:
                    //(xxx) xxxxxxx
                    //0123456789012
                    outputSB.Insert(9, "-");

                    //Finally, it is formatted correctly.
                    //(xxx) xxx-xxxx
                    PhoneNumberModel newPhoneNumModel = new PhoneNumberModel();
                    newPhoneNumModel.PhoneNumber = outputSB.ToString();

                    ViewBag.Title = "Server Side Demo";

                    return View("ServerSideDemoResult", newPhoneNumModel);
                }
            }

            //Model validation failed so return a generic error message to the form.
            else
            {
                ModelState.AddModelError("", "There were one or more errors with the phone number provided");

                ViewBag.Title = "Server Side Demo";

                return View(phoneNumModel);
            }
        }

        //
        // GET: /Home/AboutMe

        public ActionResult AboutMe()
        {
            return View();
        }
    }
}
